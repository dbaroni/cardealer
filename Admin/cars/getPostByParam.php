<?php

require "../../config.php";
require "../../authorized.php";

$param = $_GET['param'] ?? '';

if ($param != '') {
    try {
        $stmt = $db->prepare("
        SELECT Post.price, Post.km, Post.year, Post.email, Post.post_id, Post.img_id,
               Car.brand, Car.model, Post.kW, Post.liters, Car.car_id FROM Post
        LEFT JOIN Car ON Post.car_id=Car.car_id 
        HAVING model LIKE '%$param%' OR
        brand LIKE '%$param%' OR
        email LIKE '%$param%'
        ");

        $stmt->execute();
        echo json_encode($stmt->fetchAll());
        die();
    } catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }
} else {

    try {
        $stmt = $db->prepare("
        SELECT Post.price, Post.km, Post.year, Post.email, Post.post_id, Post.img_id,
           Car.brand, Car.model, Post.kW, Post.liters, Car.car_id FROM Post
        LEFT JOIN Car ON Post.car_id=Car.car_id;
        ");

        $stmt->execute();
        echo json_encode($stmt->fetchAll());
        die();
    } catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }
}

