<?php
require "../../config.php";
require "../../authorized.php";

$brand = $_GET['brand'] ?? '';

if ($brand != '') {
    try {
        $stmt = $db-> prepare("SELECT * FROM Car WHERE brand=:brand");
        $stmt->bindParam(':brand', $brand);
        $stmt->execute();
        echo json_encode($stmt->fetchAll());
    }catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }
} else {
    echo "not working";
    die();
}
?>
