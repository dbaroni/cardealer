<?php
require_once "../../config.php"; 
require_once "../../authorized.php"; 
?>

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<div id="navBar"></div>

    <div class="searchBar">
        <div>
            <input id="search-bar" size="50" class="inputTextBox" type="text" placeholder="Search...">
            <label for="search" style="position: absolute;" class="addCarText"><img src="/imgs/search.png"></label>
        </div>
    </div>
<body>
    <h2 class="upperTitle"> Found post </h2>

    <div class="addCarDiv">
        <br><br>
            <div id="post_template">
                <post v-for="car in cars" :key="car" :car="car"></post>
            </div>
    </div>

</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>