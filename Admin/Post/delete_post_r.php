<?php 
require "../../config.php";

$id = $_POST['id'] ?? '';
$id_img = $_POST['id_img'] ?? '';

try {
    $removePost = "
    DELETE FROM Post WHERE post_id='$id'
    ";

    $removeSavedPost = "
    DELETE FROM SavedPost WHERE post_id='$id'
    ";


    $stmt1 = $db-> prepare($removeSavedPost);
    $stmt1->execute();

    $stmt2 = $db-> prepare($removePost);
    $stmt2->execute();
    
    if(file_exists("../../loadedImgs/".$id_img.".png")) {
        unlink("../../loadedImgs/".$id_img.".png");
    } else if(file_exists("../../loadedImgs/".$id_img.".jpg")) {
        unlink("../../loadedImgs/".$id_img.".jpg");
    }
    
    unlink("../../loadedImgs/".$id_img.".png");

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /index.php');

?>