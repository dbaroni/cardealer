<?php

require "../../config.php";

$price = $_POST['price'] ?? '';
$km = $_POST['km'] ?? '';
$year = $_POST['year'] ?? '';
$kw = $_POST['kw'] ?? '';
$liters = $_POST['liters'] ?? '';
$brand = $_POST['brand'] ?? '';
$model = $_POST['model'] ?? '';

$email = $_SESSION['user']['email'];

if ($price == '') $price = null;
if ($km == '') $km = null;
if ($year == '') $year = null;
if ($kw == '') $kw = null;
if ($liters == '') $liters = null;

if ($price == null || $km == null || $year == '' || $kw == '' || $liters == '' || $brand == '' || $model == '') {
    $_SESSION['add_data'] =  [
        'price' => $price,
        'year' => $year ,
        'km' => $km ,
        'liters' => $liters ,
        'kw' => $kw

    ];
    header('location: /Admin/Post/add_post.php?');
    die;
}

try {
    $stmt2 = $db-> prepare("
    SELECT car_id FROM Car WHERE brand='$brand' AND model='$model'
    ");

    $stmt2->execute();
    
    if($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {    
        $car_id = $row['car_id'];
    }

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

try {

    $img_id = rand(1, 9999999);

    if (isset($_FILES['image']) && $_FILES['image']['error'] == 0 && (strpos($_FILES['image']['name'], '.jpg') || strpos($_FILES['image']['name'], '.png'))) {  //verifica png o jpg non altro
        move_uploaded_file($_FILES['image']['tmp_name'], "../../loadedImgs/$img_id.png");
        $stmt = $db-> prepare("
        INSERT INTO Post (price,km,year,kw,liters,email,car_id,img_id) VALUE(
        :price,
        :km,
        :year,
        :kw,
        :liters,
        :email,
        :car_id,
        :img_id
        )
        ");
    
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':km', $km);
        $stmt->bindParam(':year', $year);
        $stmt->bindParam(':kw', $kw);
        $stmt->bindParam(':liters', $liters);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':car_id', $car_id);
        $stmt->bindParam(':img_id', $img_id);
    
        $stmt->execute();

    } else {
        header('location: /Admin/Post/add_post.php?');
        die;
    }


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /index.php');

?>



