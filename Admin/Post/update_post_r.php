<?php

require "../../config.php";

$price = $_POST['price'] ?? '';
$km = $_POST['km'] ?? '';
$year = $_POST['year'] ?? '';
$kw = $_POST['kw'] ?? '';
$liters = $_POST['liters'] ?? '';
$id = $_POST['id'] ?? '';

if ($price == '') $price = null;
if ($km == '') $km = null;
if ($year == '') $year = null;
if ($kw == '') $kw = null;
if ($liters == '') $liters = null;

if ($price == null || $km == null || $year == null || $kw == null|| $liters == null) {
    header('location: /Admin/Post/update_post.php?');
    die;
}

try {

    $img_query = "";

    if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
        $img_id = rand(1, 9999999);
        move_uploaded_file($_FILES['image']['tmp_name'], "../../loadedImgs/$img_id.png");
        $img_query = ",img_id='$img_id' ";
    }

    $stmt = $db-> prepare("
    UPDATE Post SET price=:price,km=:km,year=:year,kw=:kw,liters=:liters ". $img_query .
    " WHERE post_id='$id' 
    ");

    $stmt->bindParam(':price', $price);
    $stmt->bindParam(':km', $km);
    $stmt->bindParam(':year', $year);
    $stmt->bindParam(':kw', $kw);
    $stmt->bindParam(':liters', $liters);

    $stmt->execute();
    

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /Admin/YourPosts/your_posts.php');

?>



