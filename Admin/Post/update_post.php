<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<?php
require "../../config.php";
require "../../authorized.php";

$id = $_POST['id'] ?? '';

$sql = "
SELECT P.price, P.km, P.year, P.liters, P.kw FROM Post P WHERE post_id='$id'
";

try {
    $stmt = $db-> prepare($sql);
    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


//database Post
$price = '';
$km = '';
$year = '';
$kw = '';
$liters = '';


?>

<div id="navBar"></div>

<br><br>
<h2 class="upperTitle">Add your Post</h2>

<div class="addCarDiv">
    <br>
    <form method="post" action="update_post_r.php" enctype="multipart/form-data">

        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
            <label for="kw" class="addCarText">kW</label>
            <input class="inputTextBox" id="kw" type="number" min="0" name="kw" size="10" maxlength="10" value="<?= $row['kw'] ?>">
            <br>

            <label for="liters" class="addCarText">CC</label>
            <input class="inputTextBox" id="liters" type="number" min="0" name="liters" size="10" maxlength="10" value="<?= $row['liters'] ?>">
            <br>

            <label for="km" class="addCarText">km</label>
            <input class="inputTextBox" id="km" type="number" step="1000" min="0" name="km" size="10" maxlength="10" value="<?= $row['km'] ?>">
            <br>

            <label for="price" class="addCarText">Price</label>
            <input class="inputTextBox" id="price" type="number" step="100" min="0" name="price" size="20" maxlength="10" value="<?= $row['price'] ?>">
            <br>

            <label for="year" class="addCarText">Year</label>
            <input class="inputTextBox" id="year" type="number" min="1900" max="2022" name="year" size="20" maxlength="50" value="<?= $row['year'] ?>">
            <br>

            <label for="image" class="addCarText">Picture</label>
            <input class="inputTextBox" id="image" type="file" name="image">
            <br>
            
            <div class="settButtonsDiv">
                <input class="settButtons" type="reset" value="Reset field">
                <input class="settButtons" type="submit" value="Update">
            </div>

            <input name="id" value="<?= $id ?>" type="hidden">
        <?php endwhile ?>
    </form>
</div>
</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>