<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<?php
require "../../config.php";
require "../../authorized.php";

$sqlCar = "
SELECT MIN(brand) AS brand FROM Car
GROUP BY brand
";

try {
    $stmtC = $db-> prepare($sqlCar);
    $stmtC->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

//database Post
$price = '';
$km = '';
$year = '';
$kw = '';
$liters = '';

?>

<div id="navBar"></div>

<br><br>
<h2 class="upperTitle">Add your Post</h2>

<div class="addCarDiv">
    <br>
    <form method="post" action="add_post_r.php" enctype="multipart/form-data">

        <label for="brand" class="addCarText">Brand</label>
        <select class="inputTextBox" name="brand" id="brand">
            <option>Seleziona una marca</option>
            <?php while($row = $stmtC->fetch(PDO::FETCH_ASSOC)): ?>
                <?php $selcd = $row['brand'] ?>
                    <option value="<?= $row['brand']; $brand = $row['brand'] ?>" <?= $selcd ?>> <?= $row['brand'] ?></option>
            <?php endwhile ?>
        </select>
        <br>

        <label for="model" class="addCarText">Model</label>
        <select class="inputTextBox" name="model" id="model">
            <option>Seleziona un modello</option>
        </select>
        <br>


        <label for="kw" class="addCarText">kW</label>
        <input class="inputTextBox" id="kw" type="number" min="0" name="kw" size="10" maxlength="10" value="<?= $kw ?>">
        <br>

        <label for="liters" class="addCarText">CC</label>
        <input class="inputTextBox" id="liters" type="number" min="0" name="liters" size="10" maxlength="10" value="<?= $liters ?>">
        <br>

        <label for="km" class="addCarText">km</label>
        <input class="inputTextBox" id="km" type="number" step="1000" min="0" name="km" size="10" maxlength="10" value="<?= $km ?>">
        <br>

        <label for="price" class="addCarText">Price</label>
        <input class="inputTextBox" id="price" type="number" step="100" min="0" name="price" size="20" maxlength="10" value="<?= $price ?>">
        <br>

        <label for="year" class="addCarText">Year</label>
        <input class="inputTextBox" id="year" type="number" min="1900" max="2022" name="year" size="20" maxlength="50" value="<?= $year ?>">
        <br>

        <label for="image" class="addCarText">Picture</label>
        <input class="inputTextBox" id="image" type="file" name="image">
        <br>
        
        <div class="settButtonsDiv">
            <input class="settButtons" type="reset" value="Reset field">
            <input class="settButtons" type="submit" value="Create">
        </div>
    </form>
</div>
</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>