<?php require_once "../../config.php"; ?>
<?php require_once "../../authorized.php"; ?>

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<div id="navBar"></div>

<?php

try {

    $email = $_SESSION['user']['email'];

    $sql = "
    SELECT Post.price, Post.km, Post.year, Post.email, Post.post_id, Post.img_id,
           Car.brand, Car.model, Post.kW, Post.liters, Car.car_id FROM Post
    LEFT JOIN Car ON Post.car_id=Car.car_id
    WHERE Post.email='$email';
    ";

    $stmt = $db-> prepare($sql);
    $stmt->execute();

} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<br><br>
<body>
    <h2 class="upperTitle"> Your Posts </h2>

    <div class="addCarDiv">
        <br><br>

        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
            <div class="post"> 

                <div class="postTitle">
                    <span class="addCarText"> <?= $row['brand'] ?> </span>
                    <span class="addCarText"> <?= $row['model'] ?> </span>
                </div>

                <div class="leftPost">
                    <?php $id=$row['img_id']; if(file_exists("../../loadedImgs/$id.png")): ?>
                        <img src="../../loadedImgs/<?= $id?>.png" width="350">
                    <?php elseif(file_exists("../../loadedImgs/$id.jpg")): ?>
                        <img src="../../loadedImgs/<?= $id?>.jpg" width="350">
                    <?php else: ?>
                        <img src="../../loadedImgs/default.png" width="350">
                    <?php endif ?>
                </div>

                <div class="rightPost">

                    <div class="postInformation">
                        <span class="addCarText"> <?= $row['kW'] ?> kW </span>
                        <span class="addCarText"> <?= $row['price'] ?> € </span>

                    </div>
                    <br>

                    <div class="postInformation">
                        <span class="addCarText"><?= $row['km'] ?> <img src="/imgs/km.png" width="15px"></img></span>
                        <span class="addCarText"><?= $row['email'] ?> <img src="/imgs/used.png" width="15px"></img></span>
                    </div>
                    <br>

                    <div class="postInformation">
                        <span class="addCarText"><?= $row['year'] ?> <img src="/imgs/calendar.png" width="15px"></img> </span>
                        <span class="addCarText"><?= $row['liters'] ?> <img src="/imgs/piston.png" width="15px"></img></span>
                    </div>
                    <br> 
                </div>

                
                    <div class="postBtn">
                        <form method="post" action="/Admin/Post/delete_post_r.php" enctype="multipart/form-data">
                            <button class="starBtn"><img width="35px" src="/imgs/delete.png"></button>
                            <input name="id" value="<?= $row['post_id']?>" type="hidden">
                            <input name="id_img" value="<?= $id?>" type="hidden">
                        </form>
                        <form method="post" action="/Admin/Post/update_post.php" enctype="multipart/form-data">
                            <button class="starBtn"><img width="35px" src="/imgs/update.png"></button>
                            <input name="id" value="<?= $row['post_id']?>" type="hidden">
                        </form>
                    </div>
            </div>
            <br><br>
        <?php endwhile ?>
    </div>
</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>