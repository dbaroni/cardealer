<?php

require "../../config.php";

$email = $_POST['email'] ?? '';
$nationality = $_POST['nationality'] ?? '';
$phone = $_POST['phone'] ?? '';
$password = $_POST['password'] ?? '';
$Md5Password = MD5($password . $salt);
$name = $_POST['name'] ?? '';
$surname = $_POST['surname'] ?? '';
$user = 'user';

if ($email == '' || $nationality == '' || $phone == '' || $password == '' || $name == '' || $surname == '') {
    header('location: /Admin/User/registration_page.php?');
    die;
}

try {
    $stmt = $db-> prepare("
    INSERT INTO User SET 
    email = :email,
    nationality = :nationality,
    phone = :phone,
    password = :password,
    `name` = :name,
    surname = :surname,
    role = :role
    ");

    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':nationality', $nationality);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':password', $Md5Password);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':surname', $surname);
    $stmt->bindParam(':role', $user);

    

    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: ../../index.php');

?>



