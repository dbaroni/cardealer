<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<?php
require "../../config.php";
require "../../authorized.php";

$email = $_SESSION['user']['email'];

$sql = "
SELECT * FROM User WHERE email='$email'
";

try {
    $stmt = $db-> prepare($sql);
    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>

<div id="navBar"></div>

<br><br>
<h2 class="upperTitle">Profile</h2>

<div class="addCarDiv">

    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

        <div class="postTitle">
            <img id="profPic" src="/imgs/avatar.png" height="50" >
            <span class="addCarText"> <?= $row['name'] ?> </span>
            <span class="addCarText"> <?= $row['surname'] ?> </span>
        </div>

        <hr style="width:40%">

        <div class="userInformation">
            <div class="profileLine">
                <label class="userText">Email:</label>
                <span class="userText"> <?= $row['email'] ?> </span><br>
            </div>
            <div class="profileLine">
                <label class="userText">Nationality:</label>
                <span class="userText"> <?= $row['nationality'] ?> </span><br>
            </div>
            <div class="profileLine">
                <label class="userText">Phone:</label>
                <span class="userText"> <?= $row['phone'] ?> </span><br>
            </div>
        </div> 

        <br>
        <div id="modProfPic">
            <a href="/Admin/User/modify_profile.php">
                <img src="/imgs/modify.png" height="50" >
            </a>
        </div>

    <?php endwhile ?>

</div>
</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>