<?php
require_once "../../config.php";

//var_export($_POST);

$email = $_POST['email'] ?? '';
$password = $_POST['password'] ?? '';

unset($_SESSION['user']);

try {
    $stmt = $db->prepare(" 
    SELECT email, role FROM User
    WHERE email=:email AND password=MD5(CONCAT(:password, :salt))
    ");

    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':salt', $salt);
    $stmt->execute();

    if ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $_SESSION['user'] = $user;
        $_SESSION['msg'] = "Sei dentro";
    } else {
        $_SESSION['msg'] = "Non puoi accedere";
    }

} catch (PDOException $e) {
    echo $e->getMessage();
    die();
}

var_dump($_SESSION['msg']);

$backto = $_SESSION['backto'] ?? '/index.php';
unset($_SESSION['backto']);

header("location: $backto");