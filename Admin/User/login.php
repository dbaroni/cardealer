<?php

require_once "../../config.php";

if (!isset($_SESSION['backto'])) {

    $backto = $_SERVER['HTTP_REFERER'] ?? '';
    if ($backto == '') {
        $backto = '/index.php';
    }
    $_SESSION['backto'] = $backto;
}

?>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<div id="navBar"></div>



<br><br>
<h2 class="upperTitle">Login</h2>

<div class="addCarDiv">
    <br>
    <form method="post" action="login_r.php">

        <label for="email" class="addCarText">Email</label>
        <input name="email" size="30" class="inputTextBox">
        <br>
        <label for="password" class="addCarText">Password</label>
        <input name="password" type="password" size="30" class="inputTextBox">
        <br>

        <div class="settButtonsDiv">
            <input class="settButtons" type="reset" value="Reset field">
            <input class="settButtons" type="submit" value="Login">
        </div>

        <div class="registrationText">
            You don't have an account?
            <a href="registration_page.php" class="registerBtn">Register here</a>
        </div>

    </form>
</div>

</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>