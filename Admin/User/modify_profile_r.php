<?php

require "../../config.php";

$nationality = $_POST['nationality'] ?? '';
$phone = $_POST['phone'] ?? '';
$name = $_POST['name'] ?? '';
$surname = $_POST['surname'] ?? '';
$email = $_SESSION['user']['email'];

if ($nationality == '' || $phone == '' || $name == '' || $surname == '') {
    header('location: /Admin/User/modify_profile_r.php?');
    die;
}

try {
    $stmt = $db-> prepare("
    UPDATE User SET 
    nationality = :nationality,
    phone = :phone,
    `name` = :name,
    surname = :surname
    WHERE email=:email
    ");

    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':nationality', $nationality);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':surname', $surname);

    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: profile.php');

?>



