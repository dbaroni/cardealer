<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>


<?php

$email = '';
$nationality = '';
$phone = '';
$password = '';
$name= '';
$surname = '';

?>

<div id="navBar"></div>

<br><br>
<h2 class="upperTitle">Registration</h2>

<div class="addCarDiv">
    <br>
    <form method="post" action="/Admin/User/registration_page_r.php" enctype="multipart/form-data">

        <label for="name" class="addCarText">Name</label>
        <input class="inputTextBox" id="name" type="text" name="name" size="30" maxlength="50" value="<?= $name ?>">
        <br>

        <label for="surname" class="addCarText">Surname</label>
        <input class="inputTextBox" id="surname" type="text" name="surname" size="30" maxlength="50" value="<?= $surname ?>">
        <br>

        <label for="nationality" class="addCarText">Nationality</label>
        <input class="inputTextBox" id="nationality" type="text" name="nationality" size="30" maxlength="50" value="<?= $nationality ?>">
        <br>

        <label for="phone" class="addCarText">Phone</label>
        <input class="inputTextBox" id="phone" type="text" name="phone" size="30" maxlength="50" value="<?= $phone ?>">
        <br>

        <label for="email" class="addCarText">Email</label>
        <input class="inputTextBox" id="email" type="text" name="email" size="30" maxlength="50" value="<?= $email ?>">
        <br>

        <label for="password" class="addCarText">Password</label>
        <input class="inputTextBox" id="password" type="text" name="password" size="30" maxlength="50" value="<?= $password ?>">
        <br>

        <div class="settButtonsDiv">
            <input class="settButtons" type="reset" value="Reset field">
            <input class="settButtons" type="submit" value="Register">
        </div>

    </form>
</div>

</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>