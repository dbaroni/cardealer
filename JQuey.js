let post = {
    props: ["car"],
    template: `

    <div class="post" id="post"> 
    
        <div class="postTitle">
            <span class="addCarText"> {{ car.brand }} </span>
            <span class="addCarText"> {{ car.model }} </span>
        </div>
        
        <div class="leftPost">
            <img :src="'/loadedImgs/' + car.img_id + '.png'" width="350">
        </div>
    
        <div class="rightPost">
            <div class="postInformation">
                <span class="addCarText"> {{ car.kW }} kW </span>
                <span class="addCarText"> {{ car.price }} € </span>
            </div>
            <br>
            <div class="postInformation">
                <span class="addCarText">{{ car.km }} <img src="/imgs/km.png" width="20"></span>
                <span class="addCarText">{{ car.email }} <img src="/imgs/used.png" width="20"></span>
            </div>
            <br>
            <div class="postInformation">
                <span class="addCarText">{{ car.year }} <img src="/imgs/calendar.png" width="20"></span>
                <span class="addCarText">{{ car.liters }} <img src="/imgs/piston.png" width="20"></span>
            </div>
            <br> 
        </div>
        
        <div class="postBtn">
            <form method="post" action="/Admin/SavedPost/saved_post_r.php" enctype="multipart/form-data">
                <button class="starBtn"><img width="35" src="/imgs/save.png"></button>
                <input name="id" :value="car.post_id" type="hidden">
            </form>
        </div>
    </div>
    <br><br>
    `
}

$("#navBar").load("/navbar_template.php");

let app =  Vue.createApp({
    components: {
        post
    },
    data() {
        return {
            cars: []
        }
    },
    methods: {
        async queryCars(query) {
            let req = await fetch('../cars/getPostByParam.php?param=' + encodeURIComponent(query));
            this.cars = await req.json();
        }
    }
});

let vm = app.mount("#post_template");

vm.queryCars($("#search-bar").val());
$("#search-bar").keyup(async function(){
    vm.queryCars($("#search-bar").val());
});

$("#brand").change(async function(){
    let models = await modelRequest($("#brand").val());
    $("#model").empty();

    models.forEach(model => {
        let option = document.createElement("option");
        option.value = model.model;
        option.textContent = model.model;
        $("#model").append(option);
    });
});

async function modelRequest(text) {
    let req = await fetch('../cars/getModelsByBrand.php?brand=' + encodeURIComponent(text));
    return await req.json();
}