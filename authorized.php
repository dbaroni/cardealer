<?php
require_once "config.php";

if (!isset($_SESSION['user'])) {

    header('location: /Admin/User/login.php');
    $_SESSION['backto'] = $_SERVER['REQUEST_URI'];
    die;

}