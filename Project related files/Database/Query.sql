CREATE TABLE Post  (
    price char(50),
    km int,
    year int,
    email char(50),
    car_id int,
    post_id int,
    PRIMARY KEY (post_id),
    FOREIGN KEY (car_id) REFERENCES Car(car_id),
    FOREIGN KEY (email) REFERENCES User(email)
);

CREATE TABLE Car (
    brand char(50),
    model char(50),
    kW int,
    liters int,
    car_id int,
    PRIMARY KEY (car_id)
);

ALTER TABLE Car MODIFY car_id int AUTO_INCREMENT;

CREATE TABLE User (
    email char(50),
    nationality char(50),
    phone char(50),
    password char(50),
    name char (50),
    surname char(50),
    PRIMARY KEY (email)
);

CREATE TABLE SavedPost (
    post_id int,
    email char(50),
    FOREIGN KEY (post_id) REFERENCES Post(post_id),
    FOREIGN KEY (email) REFERENCES User(email)
);

INSERT INTO Car VALUES ("Ford", "Mustang", 300, 2000, 1);