<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet"> 
    <title>CarDealer</title>
</head>
<body>
    <header class="navbarColor">
        <img src="imgs/logo.PNG" class="logo" width="125" height="50">
        <nav>
            <div class="navbar">
                <a href="index.php" class="navLink">Home</a>
                <a href="#htmlPage" class="navLink">Search</a>
                <a href="#htmlPage" class="navLink">Favorites</a>
                <a href="/Admin/Car/add.php" class="navLink">Add Car</a>
            </div>
        </nav>
        <a class="navLink" href="Admin/User/registrationPage.php">Login</a>
    </header> 
</body>

<?php

require "config.php";

try {
    $stmt = $db-> prepare("
    SELECT Post.price, Post.km, Post.used, Post.year
    FROM  Post
    INNER JOIN Car ON Post.car_id=Car.car_id;
    ");
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<br><br>
<body>
    <h2 class="addCarText" align="center"> Car's list </h2>

    <div class="addCarDiv">
        <br><br>

        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
            <div class="post"> 

                <div class="carImg"> <img> </img> </div>

                <div class="postTitle">  
                    <span class="addCarText"> <?= $row['brand'] ?> </span>
                    <span class="addCarText"> <?= $row['model'] ?> </span>
                </div>
                <br>

                <div class="postInformation">
                    <span class="addCarText"> <?= $row['kw'] ?> kW </span>
                    <span class="addCarText"> <?= $row['price'] ?> € </span>

                </div>
                <br>

                <div class="postInformation">
                    <span class="addCarText"><?= $row['km'] ?> <img src="/imgs/km.png" width="15px"></img></span>
                    <span class="addCarText"><?= $row['used'] ?> <img src="/imgs/used.png" width="15px"></img></span>
                </div>
                <br>

                <div class="postInformation">
                    <span class="addCarText"><?= $row['boughtyear'] ?> <img src="/imgs/calendar.png" width="15px"></img> </span>
                    <span class="addCarText"><?= $row['liter'] ?> <img src="/imgs/piston.png" width="15px"></img></span>
                </div>
                <br>

            </div>
            <br><br>
        <?php endwhile ?>
    </div>
</body>
</html>