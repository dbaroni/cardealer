<?php require_once "config.php"; ?>

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/css.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <title>CarDealer</title>
</head>

<div id="navBar"></div>

<?php

try {

    $sql = "
    SELECT Post.price, Post.km, Post.year, Post.email, Post.post_id, Post.img_id,
           Car.brand, Car.model, Post.kW, Post.liters, Car.car_id FROM Post
    LEFT JOIN Car ON Post.car_id=Car.car_id;
    ";

    $stmt = $db-> prepare($sql);
    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<br><br>
<body>
    <h2 class="upperTitle"> Home list </h2>

    <div class="addCarDiv">
        <br><br>

        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
            <div class="post"> 

                <div class="postTitle">
                    <span class="addCarText"> <?= $row['brand'] ?> </span>
                    <span class="addCarText"> <?= $row['model'] ?> </span>
                </div>

                <div class="rlpost">
                <div class="leftPost">
                    <?php $id=$row['img_id']; if(file_exists("loadedImgs/$id.png")): ?>
                        <img class="imgSize" src="/loadedImgs/<?= $id?>.png">
                    <?php elseif(file_exists("/loadedImgs/$id.jpg")): ?>
                        <img class="imgSize" src="/loadedImgs/<?= $id?>.jpg">
                    <?php else: ?>
                        <img class="imgSize" src="/loadedImgs/default.png">
                    <?php endif ?>
                </div>

                <div class="rightPost">
                    <div class="postInformation">
                        <span class="addCarText"><?= $row['email'] ?> <img src="/imgs/email.png" width="15"></span>

                    </div>
                    <br>
                    <div class="postInformation">
                        <span class="addCarText"> <?= $row['price'] ?> <img src="/imgs/euro.png" width="15"></span>
                        <span class="addCarText"><?= $row['year'] ?> <img src="/imgs/calendar.png" width="15"> </span>
                    </div>
                    <br>
                    <div class="postInformation">
                        <span class="addCarText"><?= $row['km'] ?> <img src="/imgs/km.png" width="15"></span>
                        <span class="addCarText"><?= $row['liters'] ?> <img src="/imgs/piston.png" width="15"></span>
                    </div>
                    <br>
                    <div class="postInformation">
                        <span class="addCarText"> <?= $row['kW'] ?> kW </span>
                    </div>
                    <br> 
                </div>
                </div>

                
                    <div class="postBtn">
                        <?php if(isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == "admin"): ?>
                            <form method="post" action="/Admin/Post/delete_post_r.php" enctype="multipart/form-data">
                                <button class="starBtn"><img width="35px" src="/imgs/delete.png"></button>
                                <input name="id" value="<?= $row['post_id']?>" type="hidden">
                                <input name="id_img" value="<?= $id?>" type="hidden">
                            </form>
                            <form method="post" action="/Admin/SavedPost/saved_post_r.php" enctype="multipart/form-data">
                                <button class="starBtn"><img width="35px" src="/imgs/save.png"></button>
                                <input name="id" value="<?= $row['post_id']?>" type="hidden">
                            </form>
                        <?php elseif(isset($_SESSION['user']['role'])): ?>
                            <form method="post" action="/Admin/SavedPost/saved_post_r.php" enctype="multipart/form-data">
                                <button class="starBtn"><img width="35px" src="/imgs/save.png"></button>
                                <input name="id" value="<?= $row['post_id']?>" type="hidden">
                            </form>
                        <?php endif ?>
                    </div>
            </div>
            <br><br>
        <?php endwhile ?>
    </div>
</body>
<script src="/library/vue.global.js"></script>
<script src="/library/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/JQuey.js"></script>
</html>