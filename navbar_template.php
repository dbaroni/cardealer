<?php require_once "config.php"; ?>

<body>
<header class="navbarColor">
    <a href="/index.php">
        <img src="/imgs/logo.png" class="logo" width="125" height="50" >
    </a>
    
    <nav>
        <div class="nav-container">
            <a href="/index.php" class="navLink">Home</a>
            <a href="/Admin/SearchPost/search_post.php" class="navLink">Search</a>
            <a href="/Admin/SavedPost/list_saved_post.php" class="navLink">Favorites</a>
            <a href="/Admin/Post/add_post.php" class="navLink">Add Post</a>
            <a href="/Admin/YourPosts/your_posts.php" class="navLink">Your Posts</a>
        </div>
    </nav>

    <div class="navbar">
        <?php if( isset($_SESSION['user']) ): ?>
            <a class="logout" href="/Admin/User/login_r.php">Logout</a>
            <a href="/Admin/User/profile.php">
                <img src="/imgs/avatar.png" height="50" >
            </a>
        <?php else: ?>
            <a class="navLink" href="/Admin/User/login.php">Login</a>
        <?php endif ?>
    <div class="navbar">
</header>
</body>
